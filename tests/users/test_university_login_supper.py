import json
import pytest
from datetime import date
from users.models import CustomUser, UniversityUser
from universities.models import ConsumerUnit, University
from tests.test_utils import dicts_test_utils
from tests.test_utils import create_objects_test_utils
from django.contrib.auth import authenticate
from django.test import TestCase
from django.contrib.auth.models import User

class EmailLoginTest(TestCase):
    def setUp(self):
        # Cria um usuário com e-mail em caixa baixa
        self.user = User.objects.create_user(username='usuario_teste', email='usuario@teste.com', password='senha123')

    def test_login_email_lowercase(self):
        # Tenta autenticar com o e-mail em caixa baixa
        user = authenticate(username='usuario@teste.com', password='senha123')
        self.assertIsNotNone(user)
        self.assertEqual(user.email, 'usuario@teste.com')

def test_login_email_uppercase(self):
    # Tenta autenticar com o e-mail em caixa alta
    user = authenticate(username='USUARIO@TESTE.COM', password='senha123')
    self.assertIsNotNone(user)
    self.assertEqual(user.email, 'usuario@teste.com')

def test_email_saved_as_lowercase(self):
    # Cria um usuário com o e-mail em caixa alta
    user = User.objects.create_user(username='usuario_teste2', email='USUARIO2@TESTE.COM', password='senha123')
    # Verifica se o e-mail foi salvo em caixa baixa
    self.assertEqual(user.email, 'usuario2@teste.com')


